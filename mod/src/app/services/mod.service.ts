import { Injectable } from '@angular/core';
import { IpcRenderer,IpcMain } from 'electron';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ModService {

  Light!:String;

  private ipcRenderer=window.require('electron').ipcRenderer;

  constructor() { 
    
  }


  getStatus(){    //取得溫溼度、燈的狀態

    return this.ipcRenderer.sendSync("getstatus")

  }

  setLight(P:String){ //傳送燈的狀態
    this.Light=P;

    this.ipcRenderer.send('Light',P); 
    
  }


}

