import { Component, OnInit } from '@angular/core';
import { ModService } from 'src/app/services/mod.service';
import { IpcRenderer, ipcMain } from 'electron';
// import { IpcRenderer } from 'electron/renderer';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  panelOpenState = false;
  panelOpenState1 = false;
  isChecked = false;
  custom_Bool = true;
  customHum_Num = 60;
  customDim_Num = 255;

  checkHum_Num!: number;
  Dim_Num!: number;


  private ipcRenderer = window.require('electron').ipcRenderer;

  constructor(
    private mod: ModService,
  ) {

  }

  Status: any = [];

  H!: String;
  T!: String;
  L!: String;
  Dim!: String;

  ngOnInit(): void {
    setInterval(() => {
      //取得資料
      this.Status = this.mod.getStatus();
      //如果還沒有取的資料的話，則顯示載入中
      if (this.Status[0] == undefined || this.Status[1] == undefined || this.Status[2] == undefined) {
        this.H = "Loading!";
        this.T = "Loading!";
        this.L = "Loading!";
      } else {
        //取得資料後傳到顯示的字
        this.H = this.Status[0] + "%";
        this.T = this.Status[1] + "C";
        this.L = this.Status[2];
      }
      //確認燈是不是開著的
      if (this.L == "Light OFF") {
        this.isChecked = false;
      } else {
        this.isChecked = true;
      }

      //確認使用者是否要自訂相關的資訊
      if (this.custom_Bool == true) {
        //如果打勾，但沒有填入資料的話，還是使用預設值
        if (this.customHum_Num == undefined || this.Dim_Num == undefined) {
          this.checkHum_Num = 60;
          this.Dim_Num = 255;
        } else {
          this.checkHum_Num = this.customHum_Num;
          this.Dim_Num = this.customDim_Num;
        }
      } else {
        this.checkHum_Num = 60;
        this.Dim_Num = 255;
      }
      //如果濕度達到設定值，則調整燈的亮度
      if (Number(this.Status[0]) >= this.checkHum_Num) {
        this.Dim = String(this.Dim_Num);
      }

      //如果燈是亮的，則傳送對應的亮度
      if (this.isChecked == true) {
        console.log("Hum:", this.checkHum_Num, "Dim:", this.Dim_Num);
        this.ipcRenderer.send('Wet', this.Dim);
      }
    }, 5000);
  }
  Light() {
    //判斷燈的開關
    if (this.isChecked == true) {
      this.mod.setLight("0");
    } else {
      this.mod.setLight("1");
    }
  }

}
