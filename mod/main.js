const { app, BrowserWindow, ipcMain, ipcRenderer } = require('electron')
const url = require("url");
const path = require("path");


let mainWindow

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, `/dist/mod/index.html`),
      protocol: "file:",
      slashes: true
    })
  );
  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function () {
    clientLED.writeRegister(9999, "0");
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})

var Modbus = require('modbus-serial');
var client = new Modbus();
var clientLED = new Modbus();

client.connectTCP('192.168.11.30', { port: 502 });  //連線到D1的位置
clientLED.connectTCP('192.168.11.94', { port: 502 });//連線到P2的位置


var H;
var T;
var L;

setInterval(function () {

  //讀取溫溼度的資訊
  client.readHoldingRegisters(0, 2, function (err, data) {

    if (data == undefined) {
      console.log("Cant't Read Data");
    } else {
      H = data.data[0] / 10;

      T = data.data[1] / 10;
    }
  });

  //讀取LED的資訊
  clientLED.readHoldingRegisters(9999, 1, function (err, data) {
    if (data.data[0] == "1") {
      L = "Light ON";
    } else {
      L = "Light OFF";
    }
  });

  //傳送資料給Angular Service
  ipcMain.on('getstatus', (event, arg) => {
    event.returnValue = [H, T, L];
  })

}, 5000);

//修改燈的開關
ipcMain.on('Light', (event, data) => {
  clientLED.writeRegister(9999, data);
});

//修改溫溼度造成的亮度
ipcMain.on('Wet', (event, data) => {
  clientLED.writeRegister(10000, data);
});
