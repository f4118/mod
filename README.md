# Mod

連接到 Modbus 設備 並利用Angular及Electron來顯示資料

## 安裝

1. 安裝需要的內容
```sh
npm install
```

2. 安裝Electron
```sh
npm install electron@latest --save-dev
```

3. 安裝 Angular Material
```sh
ng add @angular/material
```

4. Done!

